Welcome to W4 Cloud's documentation!
====================================

.. warning:: This documentation is a **work in progress**! If you notice any errors,
             missing information or other issues or have any question, please
             `open an issue <https://github.com/W4-Cloud/w4gd>`_ or
             `contact us <https://w4games.com/#contact>`_.

W4 Cloud services are designed to assist during the entire development process,
and streamline releasing your game to the general public without having to
engineer complex network infrastructure.

This includes:

- **Flexible hosting:**
  Peer-to-peer networking (P2P) with signaling server or
  on-demand hosted scalable authoritative servers.

- **Programmable serverless backend:**
  Including database functionality with real-time event support and
  hosting and server fleet dashboards and configuration.

- **Lobbies & matchmaking.**

- **User and player management:**
  Authentication with platform accounts, moderation.

- **Data storage for both game and player data:**
  Leaderboards, player profiles, persistent world data, etc.

- **Resource storage:**
  Game assets, DLCs, user generated content, etc.

- **Marketing & performance analytics.**


.. Below is the main table-of-content tree of the documentation website.
   It is hidden on the page itself, but it makes up the sidebar for navigation.

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: About
   :name: sec-general

   about/introduction

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Getting Started
   :name: sec-learn

   getting_started/installation
   getting_started/authentication
   getting_started/database/index
   getting_started/matchmaker/index
   getting_started/dedicated_servers/index

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Tutorials
   :name: sec-tutorials

   tutorials/getting_started/index

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: API Reference
   :name: sec-class-reference

   api/index
