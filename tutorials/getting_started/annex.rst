Annex
=====

How to access server logs
-------------------------

You can access the logs for debugging on the "logs" tab of your
dashboard

.. image:: images/w4-logs.png

Press "live logs" to see it refresh in real-time. You can also filter
the logs by using the filter box.

Beta and Staging Releases
-------------------------

The W4 builds/Fleets gives you the tooling to create different
concurrent versions of your game.

You can upload different builds to different fleets.

As long as your build system can create a global constant with a version
number, then you can add that in your lobby creation and your lobbies
listing.

Differences in the final project on GitHub
------------------------------------------

The `GitHub repository <https://github.com/W4-Cloud/gdquest-tutorials>`__ 
for this project has some notable changes compared to this tutorial:

1. All the functions are as strongly typed as possible.
2. We have a few little helpers, such as code to differentiate a server
   debug instances, and a command line argument parser.
3. We have a plugin allowing to run multiple games that connect to W4 
   servers.
4. We use local generation of user and password, so we can switch user
   profiles.
5. Android builds.
6. A synchronizer to copy server logs locally so you can observe them.

And a lot more.

We exhaustively commented the demo, so please do feel welcome to browse
the repository and explore the source code.

Uploading a game build manually
-------------------------------

Here's how you would upload your game build manually to W4. Doing this can help
explain how the relationship between builds and fleets works.

.. note::

   This is completely optional! It's an alternative method to
   using the in-editor helper. You can skip it and come back to it
   later.

First, export your game in Godot. Then, open your web browser.

Login to your W4 dashboard, and select Builds in the menu on the left.

.. image:: images/w4-configuration-builds-010.png

Create a new Build by clicking the button in the top-right.

.. image:: images/w4-configuration-builds-020-new-button.png

Name your build something sensible, then click the Select button to open the file browser.

.. image:: images/w4-configuration-builds-030-new-build-dialog.png

Click the Upload files button in the top-right.

.. image:: images/w4-configuration-builds-040-new-build-dialog-select-file.png

In the popup, click Browse... to open the file browser. Then, browse to the location of 
your Godot export, open the file, and click the Save button.

.. image:: images/w4-configuration-builds-050-new-build-dialog-select-file-upload.png

Find your newly uploaded file in the file list (if it's your first build, you'll only have
only one file).

Then, click the Select button in the bottom-right.

.. image:: images/w4-configuration-builds-060-new-build-dialog-select-file-done.png

Click the Save changes button.

.. image:: images/w4-configuration-builds-070-new-build-dialog-save-changes.png

Go to the Fleets page by clicking Fleets in the menu on the left.

.. image:: images/w4-configuration-fleets-01.png

Click the edit icon to the right of your fleet, in the Actions column.
It's an icon with a pencil over a sheet of paper.

.. image:: images/w4-configuration-fleets-02-edit.png

In the Build dropdown menu, select the build you just uploaded, and click Save.

.. image:: images/w4-configuration-fleets-03-save.png

There you go! You now know how to upload builds manually.

Known Bugs
----------

saml relaystate comes from another ip address
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/error_saml_relaystate_comes_from_another_ip_address.png
   :alt: An error that says "saml relaystate comes from another ip address"

   An error that says "saml relaystate comes from another ip address"

You might encounter this sometimes; this is a W4 bug that is being
worked on. Ignore it, and restart the game when that happens.

More Resources
--------------

Below are a few collected quality links to learn more about networking.

-  `Snapnet Blog <https://snapnet.dev/blog/>`__
-  `Explainer Series: Authoritative Servers, Relays & Peer-To-Peer -
   Understanding Networking Types and their Benefits for each Game
   Types <https://edgegap.com/blog-en/explainer-series-authoritative-servers-relays-peer-to-peer-understanding-networking-types-and-their-benefits-for-each-game-types>`__
-  `Authoritative Server FAQ \| Photon
   Engine <https://doc.photonengine.com/bolt/current/troubleshooting/authoritative-server-faq>`__
-  `Determinism in League of Legends: Implementation \| Riot Games
   Technology <https://technology.riotgames.com/news/determinism-league-legends-implementation>`__
-  `willardf/Hazel-Networking: Hazel Networking is a low level
   networking library for C# providing connection-oriented,
   message-based communication via
   RUDP. <https://github.com/willardf/Hazel-Networking>`__
-  `1500 Archers on a 28.8: Network Programming in Age of Empires and
   Beyond <https://www.gamedeveloper.com/programming/1500-archers-on-a-28-8-network-programming-in-age-of-empires-and-beyond>`__
-  `awesome/docs/awesome/Awesome-Game-Networking.md at master ·
   icopy-site/awesome <https://github.com/icopy-site/awesome/blob/master/docs/awesome/Awesome-Game-Networking.md>`__

Licenses
========

All scripts are under MIT license, credited to W4 unless specified
otherwise in the file's header.

All assets are CC-by-SA and should be credited to GDQuest except the
sounds, which are by TheRandomSoundByte2637 and Kevin MacLeod.

Tutorial icons courtesy of https://lucide.dev and https://copyicon.com/

