:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/rest-client/client_async_result.gd.

.. _class_addons_w4gd_rest-client_client_async_result_gd:

res://addons/w4gd/rest-client/client_async_result.gd
====================================================

**Inherits:** :ref:`res://addons/w4gd/rest-client/client_result.gd<class_addons_w4gd_rest-client_client_result_gd>` **<** RefCounted

The result of an async HTTP request.

.. rst-class:: classref-reftable-group

Properties
----------

.. table::
   :widths: auto

   +---------+-------------------------------------------------------------------------------------------------------+----------+
   | Variant | :ref:`pending_request<class_addons_w4gd_rest-client_client_async_result_gd_property_pending_request>` | ``null`` |
   +---------+-------------------------------------------------------------------------------------------------------+----------+

.. rst-class:: classref-reftable-group

Methods
-------

.. table::
   :widths: auto

   +------+-----------------------------------------------------------------------------------------------+
   | void | :ref:`cancel<class_addons_w4gd_rest-client_client_async_result_gd_method_cancel>` **(** **)** |
   +------+-----------------------------------------------------------------------------------------------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Signals
-------

.. _class_addons_w4gd_rest-client_client_async_result_gd_signal_completed:

.. rst-class:: classref-signal

**completed** **(** **)**

Emitted when the request is complete.

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Property Descriptions
---------------------

.. _class_addons_w4gd_rest-client_client_async_result_gd_property_pending_request:

.. rst-class:: classref-property

Variant **pending_request** = ``null``

The pending HTTP request.

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Method Descriptions
-------------------

.. _class_addons_w4gd_rest-client_client_async_result_gd_method_cancel:

.. rst-class:: classref-method

void **cancel** **(** **)**

Cancels the pending request.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
