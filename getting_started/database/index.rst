.. _doc_getting_started_database:

Database (Supabase)
===================

.. warning:: Just an outline!

- EXPLAIN: We've got this great thing called Supabase - it's Open Source!

.. toctree::
   :maxdepth: 1

   w4rm
   storage
   realtime
   examples/index

