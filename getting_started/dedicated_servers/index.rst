.. _doc_getting_started_dedicated_servers:

Dedicated Servers
=================

Rather than managing your own server infrastructure, you can host dedicated game servers on the W4 Cloud!

You can easily manage fleets of game servers across multiple regions world-wide, and scale the number of servers in response to player demand.

.. toctree::
   :maxdepth: 1

   overview
   adapting_your_game
   uploading
   fleet_setup
