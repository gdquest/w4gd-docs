.. _doc_getting_started_installation:

Installation
============

The first step to using W4 Cloud services in your project is to add the
`W4GD GDScript SDK <https://github.com/W4-Cloud/w4gd>`_ Godot addon.
You can find the latest release at
`https://github.com/W4-Cloud/w4gd/releases/latest <https://github.com/W4-Cloud/w4gd/releases/latest>`_.

Requirements
------------

The W4GD GDScript SDK requires the following to access most features:

- Godot Engine 4.0+
- Network access to W4 backend servers
- The *Anonymous API key* provided in the project dashboard (under **DOCS > API** in the menu)

Copy W4GD into your project
---------------------------

The quick and easy way is to download W4GD from the repository:

#. Browse to the release you want to install
   (for example, `latest <https://github.com/W4-Cloud/w4gd/releases/latest>`_)
#. Locate the **w4gd.zip** download link at the bottom
   (`latest w4gd.zip <https://github.com/W4-Cloud/w4gd/releases/latest/download/w4gd.zip>`_)
#. Download and unzip **w4gd.zip**
#. Move the contained folder to Godot's **addons/** folder
   (you may need to create the **addons/** folder in your project root folder if
   you haven't installed any addons yet)
#. The addon should now live at **addons/wg4d/**,
   you may want to rename the folder to **w4gd** if it was named **w4gd-main**
   or similar in the download

Alternatively, you can add the W4GD addon as a `Git Submodule <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_.

Either way, you may want to make note of the W4GD version you are now using.
If you run into issues, checking whether an updated SDK containing bugfixes is
available can be helpful.

Consider committing your addons to version control
software to easily track them (also known as *vendoring dependencies*).

Enable W4GD addon
-----------------

After you copied **addons/wg4d/** into your project as described above,
start the Godot Editor. Open **Project > Project Settings** and locate the
**Plugins** tab. You should see the **W4GD** addon listed. Set it to **Enabled**.

.. image:: img/install_project_manager.webp
   :alt: Screenshot of the Project Manager with the W4GD addon enabled

Set URL and key in W4 dock
--------------------------

Once the W4GD addon is enabled, the **W4 dock** should be added to the editor UI,
right next to the **Scene** tab. Navigate to the **W4 dock**.

.. image:: img/install_w4_dock.webp
   :alt: Screenshot of the W4 dock

The **W4 dock** allows you to manage various settings like API keys or
which servers and databases to connect to in multiple profiles.
Different profiles allow you to set different settings for example for
*live* or *production* servers and *test* or *development* servers,
so your project connects to different backend systems depending on the profile.

For now, you want to edit the *Default* profile and add the URL and
API key to your backend. Note that the settings are *locked* by default
and can only be edited after unlocking them by clicking on the lock icon.

#. Unlock the *default* settings by clicking on the lock icon
   (it should switch to an unlocked lock icon and the settings should become editable)
#. Enter the **URL** to your backend (in the form of ``https://<project>.w4games.cloud/``)
#. Enter the **Anonymous API key** for your backend
   (you can find the API keys under **DOCS > API** in your dashboard, or
   browse directly to ``https://<project>.w4games.cloud/dashboard/docs/api/``,
   see also *About API keys* below)
#. Re-lock the settings by clicking the lock once again and save your project.

.. image:: img/install_w4_dock_filled.webp
   :alt: Screenshot of the W4 dock with filled in URL and API key

About API keys
--------------

You generally want to use the **Anonymous API key**.
This API key is intended to have the least privileges. The intention is
for users to use it to then authenticate and be granted a more privileged
personal key that is allowed to access (and change) more data.

The **Service API key** is privileged and should only be used for administrative
tasks.

.. warning:: Make sure to never distribute your **Service API key**.
             Do not include it in your distributed project's code,
             avoid putting in in your version control system.

             **Handle the Service API key like an admin password or secret, because it is.**
